# ifscube

A set of python scripts and functions to analyse and process integral
field spectroscopy data cubes.

Author: Daniel Ruschel Dutra

Website: https://bitbucket.org/danielrd6/ifscube/

## Installation instructions

We recommend installing the package via pip (https://pypi.python.org/pypi/pip),
which is bundled with the standard Python distribution since Python 3.4.

To install IFSCUBE using pip, you must first intall git (https://git-scm.com/),
and then just enter the following in a terminal:

        pip install git+https://danielrd6@bitbucket.org/danielrd6/ifscube.git